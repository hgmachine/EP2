package tocomfomequeromais;

import javax.swing.JFrame;

public class ToComFomeQueroMais {

    private SystemDateTime today;
    
    public static void main(String[] args)
    throws InterruptedException
    {
        
        Manager mainwindow= new Manager();        
        mainwindow.setResizable( false );
        mainwindow.setVisible( true );
        
        // Thread - Relógio e Data
        SystemDateTime today= new SystemDateTime();
        Thread threadDateTime = new Thread(today);
        threadDateTime.start();        
    }
}