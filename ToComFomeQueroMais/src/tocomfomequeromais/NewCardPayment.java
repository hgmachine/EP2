package tocomfomequeromais;

import datamodel.CardPayment;
import javax.swing.JOptionPane;

public class NewCardPayment extends javax.swing.JFrame {

    private Double value;
    private CardPayment pagamento;
       
    public NewCardPayment() {
        initComponents();        
        pagamento= new CardPayment();
        jTextFieldCardCode.setText(null);
        jTextFieldCardName.setText(null);
        jTextFieldCardNumber.setText(null);
        this.setValue(Double.valueOf("0"));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCardName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldCardNumber = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldCardCode = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jButtonCancelar = new javax.swing.JButton();
        jButtonRealizar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Insira as informações requeridas para o cartão:");

        jLabel2.setText("Nome do cartão:");

        jLabel3.setText("Número do cartão:");

        jTextFieldCardCode.setToolTipText("");

        jLabel4.setText("Código de segurança do cartão: ");

        jButtonCancelar.setText("Cancelar Pagamento");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonRealizar1.setText("Realizar Pagamento");
        jButtonRealizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRealizar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 111, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldCardCode))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldCardNumber)
                            .addComponent(jTextFieldCardName)))
                    .addComponent(jButtonRealizar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCardName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCardNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCardCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonRealizar1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void setValue(Double value)
    {
        this.value= value;
    }
    
    public void setCupom(boolean cupom)
    {
        pagamento.setCupom(cupom);
    }
    
    private boolean validarDados()
    {
        if (jTextFieldCardName.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "O nome não foi informado!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldCardName.requestFocus();
            return false;
        }
        
        if (jTextFieldCardNumber.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "O número do cartão não foi informado!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldCardNumber.requestFocus();
            return false;
        }
        
        if (jTextFieldCardCode.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "O código de segurança do cartão não foi informado!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldCardCode.requestFocus();
            return false;
        }
        
        return true;
    }
    
    private void jButtonRealizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRealizar1ActionPerformed
        if (this.validarDados() == false) {
            return;
        }                
        pagamento.setCardname(jTextFieldCardName.getText());
        pagamento.setCardnumber(jTextFieldCardNumber.getText());
        pagamento.setSecuritycode(jTextFieldCardCode.getText());
        pagamento.setValue(Double.valueOf(Manager.jTextFieldPrice.getText()));
        pagamento.setPayment(Double.valueOf(Manager.jTextFieldPrice.getText()));
        this.setVisible(false);        
        if (pagamento.getCupom())
            JOptionPane.showMessageDialog(null, "Retire seu cupom fiscal.", "Operação",JOptionPane.PLAIN_MESSAGE);        
        Manager.limparCampos();
        Manager.addOrder();
    }//GEN-LAST:event_jButtonRealizar1ActionPerformed

    public CardPayment getPayment()
    {
        return pagamento;
    }
    
    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        if (this.validarDados() == false) {
            return;
        }                
        pagamento.setCardname(jTextFieldCardName.getText());
        pagamento.setCardnumber(jTextFieldCardNumber.getText());
        pagamento.setSecuritycode(jTextFieldCardCode.getText());
        pagamento.setValue(value);
        pagamento.setPayment(Double.valueOf("0"));
        this.setVisible(false);        
        JOptionPane.showMessageDialog(null, "Esta operação gerou um débito para o cliente.", "Operação",JOptionPane.PLAIN_MESSAGE);        
        Manager.limparCampos();
        Manager.addOrder();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewCardPayment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewCardPayment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewCardPayment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewCardPayment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewCardPayment().setVisible(true);
            }
        });
    }

    public void limparCampos()
    {
        jTextFieldCardCode.setText(null);
        jTextFieldCardName.setText(null);
        jTextFieldCardNumber.setText(null);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonRealizar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldCardCode;
    private javax.swing.JTextField jTextFieldCardName;
    private javax.swing.JTextField jTextFieldCardNumber;
    // End of variables declaration//GEN-END:variables
}
