package tocomfomequeromais;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.SwingWorker;
 
public class SystemDateTime extends Thread {
    
    private static Calendar calendar;
    private static SimpleDateFormat date_out;
    private static Date date;
    
    public SystemDateTime()
    {
        calendar = new GregorianCalendar();
        date_out = new SimpleDateFormat("dd/MM/yyyy");         
    }    
    
    public void run()            
    {
        try {
            for(;;)
            {
                Calendar now;
                int hour, minute, second, time;
                hour= calendar.get(Calendar.HOUR_OF_DAY);
                minute= calendar.get(Calendar.MINUTE);
                second= calendar.get(Calendar.SECOND);
                date = new Date();
                calendar.setTime(date);
                Manager.setCalendar(date_out.format(calendar.getTime()),hour,minute,second);
                // Causes this thread to sleep for 1000 miliseconds
                Thread.sleep(1000);
            }
        } catch (InterruptedException exception) {
            System.out.println(exception);                    
        }
    }
}
