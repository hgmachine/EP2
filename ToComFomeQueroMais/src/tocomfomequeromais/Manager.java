package tocomfomequeromais;

import controle.Checkout;
import datamodel.CardPayment;
import datamodel.Client;
import datamodel.Employee;
import datamodel.Order;
import datamodel.Register;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import datamodel.Payment;
import datamodel.Product;
import datamodel.Stock;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

public class Manager extends javax.swing.JFrame {

    static public Stock dispensa;    
    static private Double value;    
    static private Register ordens;    
    static private Diary caderno;
    static private Order currentOrdem;
    static private String paymentCondition;
    static private NewCashPayment money;
    static private NewCardPayment cartao;
    static private Checkout controle;
    static private Client currentCliente;
    
    private NewOrder select;
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public Manager() {
        initComponents();
        
        this.currentOrdem= new Order();
        this.select= new NewOrder();
        this.ordens= new Register(0,0);
        this.currentCliente= new Client();
        this.cartao= new NewCardPayment();
        this.money= new NewCashPayment();
        this.controle= new Checkout();
        this.caderno= new Diary();
        select.setVisible(false);
        cartao.setVisible(false);
        money.setVisible(false);
        caderno.setVisible(false);
        value= Double.valueOf("0");
        limparCampos();        
        desabilitarCampos();        
        paymentCondition= "NONE";        
        this.dispensa= new Stock(10,10,10,10,10,10,10,10,10,10,10,10,10,10,10);
        money.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
        cartao.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );        
        select.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
        caderno.setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );        
        jTextFieldOK.setText(Integer.toString(ordens.getNumberOfOK()));
        jTextFieldAsk.setText(Integer.toString(ordens.getNumberOfSales()));        
    }    

    static protected void addOrder()
    {
        delivery();
        if (paymentCondition=="CASH")
            currentOrdem.setCashPayment(money.getPayment());
        else
            currentOrdem.setCardPayment(cartao.getPayment());
        if (jRadioButtonNotaSim.isSelected())
            JOptionPane.showMessageDialog(null, "Retire seu cupom fiscal.", "Operação",JOptionPane.PLAIN_MESSAGE);        
        ordens.addOrder(currentOrdem);
        caderno.note(currentOrdem);
        jTextFieldOK.setText(Integer.toString(ordens.getNumberOfOK()));
        jTextFieldAsk.setText(Integer.toString(ordens.getNumberOfSales()));        
        currentOrdem= new Order();
        currentOrdem.clear();        
    }
    
    static private void delivery()
    {
        controle.subtractItens();
    }
    
    static protected void limparCampos() {
        desabilitarPagamentos();        
        value= Double.valueOf("0");
        jTextFieldClient.setText(null);
        jTextFieldVender.setText(null);
        jTextFieldNiver.setText(null);   
        checkboxDesconto.setState(false);
        jRadioButton5Percent.setSelected(true);
        jRadioButton5Percent.setEnabled(false);
        jRadioButton10Percent.setEnabled(false);        
        jTextFieldPrice.setText(Double.toString(value));        
    }
    
    static protected void habilitarPagamentos()
    {
        jButtonPayCard.setEnabled(true);
        jButtonPayCash.setEnabled(true);
    }
    
    static protected void desabilitarPagamentos()
    {
        jButtonPayCard.setEnabled(false);
        jButtonPayCash.setEnabled(false);
    }
          
    private void desabilitarCampos()
    {
        jButtonAsk.setEnabled(false);
        jButtonCancel.setEnabled(false);
        jButtonEdit.setEnabled(false);
        jButtonCheckStock.setEnabled(false);
    }
    
    private void habilitarCampos()
    {
        jButtonAsk.setEnabled(true);
        jButtonCancel.setEnabled(true);
        jButtonEdit.setEnabled(true);
        jButtonCheckStock.setEnabled(true);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabelHora = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldHour = new javax.swing.JTextField();
        jTextFieldDate = new javax.swing.JTextField();
        jTextFieldAsk = new javax.swing.JTextField();
        jTextFieldOK = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldVender = new javax.swing.JTextField();
        jTextFieldClient = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldNiver = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jRadioButton5Percent = new javax.swing.JRadioButton();
        jRadioButton10Percent = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jButtonPayCard = new javax.swing.JButton();
        jButtonPayCash = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jRadioButtonNotaSim = new javax.swing.JRadioButton();
        jRadioButtonNotaNao = new javax.swing.JRadioButton();
        jTextFieldPrice = new javax.swing.JTextField();
        jButtonHistory = new javax.swing.JButton();
        jButtonNew1 = new javax.swing.JButton();
        checkboxDesconto = new java.awt.Checkbox();
        jPanel4 = new javax.swing.JPanel();
        jButtonAsk = new javax.swing.JButton();
        jButtonEdit = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonCheckStock = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableLista = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tocomfomequeromais/LOGO.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setFocusTraversalPolicyProvider(true);

        jLabelHora.setText("HORA:");

        jLabel3.setText("DATA:");

        jLabel4.setText("Nº PEDIDOS:");

        jLabel5.setText("REALIZADOS:");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("==== SISTEMA ====");

        jTextFieldHour.setEditable(false);

        jTextFieldDate.setEditable(false);

        jTextFieldAsk.setEditable(false);

        jTextFieldOK.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelHora)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldHour)
                            .addComponent(jTextFieldDate)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAsk)
                            .addComponent(jTextFieldOK))))
                .addContainerGap())
            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelHora)
                    .addComponent(jTextFieldHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldAsk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("Vendedor:");

        jLabel7.setText("Cliente:");

        jLabel8.setText("Aniversário: ");

        jLabel9.setText("Desconto:");

        buttonGroup1.add(jRadioButton5Percent);
        jRadioButton5Percent.setText("5%");
        jRadioButton5Percent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton5PercentActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton10Percent);
        jRadioButton10Percent.setText("10%");
        jRadioButton10Percent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton10PercentActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonPayCard.setBackground(new java.awt.Color(255, 255, 255));
        jButtonPayCard.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tocomfomequeromais/CARD.gif"))); // NOI18N
        jButtonPayCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPayCardActionPerformed(evt);
            }
        });

        jButtonPayCash.setBackground(new java.awt.Color(255, 255, 255));
        jButtonPayCash.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tocomfomequeromais/CASH.gif"))); // NOI18N
        jButtonPayCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPayCashActionPerformed(evt);
            }
        });

        jLabel11.setText("Total a pagar:");

        jLabel12.setText("Gerar nota fiscal?");

        buttonGroup2.add(jRadioButtonNotaSim);
        jRadioButtonNotaSim.setText("Sim");

        buttonGroup2.add(jRadioButtonNotaNao);
        jRadioButtonNotaNao.setSelected(true);
        jRadioButtonNotaNao.setText("Não");

        jTextFieldPrice.setEditable(false);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonPayCash, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonPayCard, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel11)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldPrice))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jRadioButtonNotaSim)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jRadioButtonNotaNao))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextFieldPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jRadioButtonNotaSim)
                    .addComponent(jRadioButtonNotaNao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonPayCash)
                    .addComponent(jButtonPayCard))
                .addContainerGap())
        );

        jButtonHistory.setText("Histórico");
        jButtonHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHistoryActionPerformed(evt);
            }
        });

        jButtonNew1.setText("Cancelar + Novo Pedido");
        jButtonNew1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNew1ActionPerformed(evt);
            }
        });

        checkboxDesconto.setLabel("Sim");
        checkboxDesconto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkboxDescontoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jTextFieldNiver, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(checkboxDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(jRadioButton5Percent)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jRadioButton10Percent))
                            .addComponent(jTextFieldClient)
                            .addComponent(jTextFieldVender)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jButtonNew1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextFieldVender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jRadioButton5Percent)
                        .addComponent(jRadioButton10Percent))
                    .addComponent(jLabel8)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldNiver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addComponent(checkboxDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonNew1, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(jButtonHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButtonAsk.setText("Finalizar Pedido");
        jButtonAsk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAskActionPerformed(evt);
            }
        });

        jButtonEdit.setText("Editar");
        jButtonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditActionPerformed(evt);
            }
        });

        jButtonCancel.setText("Cancelar");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        jButtonCheckStock.setText("Verificar Estoque");
        jButtonCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCheckStockActionPerformed(evt);
            }
        });

        jTableLista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableLista);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAsk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonAsk)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCheckStock)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public static void setCalendar(String date_out,int hour, int minute, int second)
    {
        String hours, minutes, seconds;
        
        if (hour<10) {
            hours= "0"+hour;            
        } else {
            hours= String.format("%d",hour);
        }

        if (minute<10) {
            minutes= "0"+minute;            
        } else {
            minutes= String.format("%d",minute);
        }

        if (second<10) {
            seconds= "0"+second;            
        } else {
            seconds= String.format("%d",second);
        }
        
        String clock_out= String.format("%sh%sm%ss",hours,minutes,seconds);
        jTextFieldDate.setText(date_out);
        jTextFieldHour.setText(clock_out);
    }
        
    private void jButtonAskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAskActionPerformed
        String item;
        String preferencia;        
        currentOrdem.clear();
        Employee vendedor= new Employee();
        currentCliente.clear();
        
        if (this.validarInformacoes() == false) {
            return;
        }
         
        vendedor.setName(jTextFieldVender.getText());
        currentCliente.setName(jTextFieldClient.getText());
        currentCliente.setNiver(jTextFieldNiver.getText());
        currentCliente.setDesconto(Double.valueOf("0.95"));
        currentCliente.setDebt(Double.valueOf("0"));
                        
        currentOrdem.setVender(vendedor);
        currentOrdem.setCliente(currentCliente);        
        currentOrdem.setDate(jTextFieldDate.getText());
        currentOrdem.setTime(jTextFieldHour.getText());
        
        DefaultTableModel model= (DefaultTableModel) jTableLista.getModel();
        for (int i=0;i<model.getRowCount();i++ )
        {
            item= model.getValueAt(i, 0).toString();
            preferencia= model.getValueAt(i, 1).toString();
            value+= getPrice(item);
            if (value > 0)
            {
                Product produto= new Product();            
                produto.insertGeneralItem(item, preferencia);
                produto.setPrice(getPrice(item));
                currentOrdem.addProduct(produto);
            }
        }
        
        if (value > 0)
        {
            JOptionPane.showMessageDialog(null, "Pedido Finalizado! Escolha uma das formas de pagamento.", "Operação",JOptionPane.PLAIN_MESSAGE);
            habilitarPagamentos();
        }
        else
            JOptionPane.showMessageDialog(null, "O pedido deve constar pelo menos um item! Realize novamente o pedido.", "Operação",JOptionPane.PLAIN_MESSAGE);             

        jTextFieldPrice.setText(Double.toString(value));
        select.reset();
        desabilitarCampos();
    }//GEN-LAST:event_jButtonAskActionPerformed

    private void cancelarPedido()
    {
        this.limparCampos();
        select.reset();
        select.setVisible(false);
        desabilitarCampos();
    }
    
    private Double getPrice(String name)
    {
        Double price= Double.valueOf("0");
        switch (name) {
            case "Hamburger":
                price= Double.valueOf("25");
                break;
            case "Pizza":
                price= Double.valueOf("45");
                break;
            case "Carne":
                price= Double.valueOf("18");
                break;
            case "Massa":
                price= Double.valueOf("15");
                break;
            case "Peixe":
                price= Double.valueOf("32");
                break;
            case "Cerveja":
                price= Double.valueOf("5");
                break;
            case "Suco":
                price= Double.valueOf("4");
                break;
            case "Coke":
                price= Double.valueOf("4");
                break;
            case "Agua":
                price= Double.valueOf("3");
                break;
            case "Vodka":
                price= Double.valueOf("12");
                break; 
            case "Bolo":
                price= Double.valueOf("8");
                break;
            case "Torta":
                price= Double.valueOf("22");
                break;
            case "Trufa":
                price= Double.valueOf("3");
                break;
            case "Sorvete":
                price= Double.valueOf("10");
                break;
            case "Gelatina":
                price= Double.valueOf("2");
                break;
         }
        return price;
    }
    
    private void jButtonPayCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPayCashActionPerformed
        money.setVisible(true);
        paymentCondition= "CASH";
        currentCliente.setPaymentCondition(paymentCondition);
        money.limparCampos();
        money.setValue(Double.valueOf(jTextFieldPrice.getText()));
        if (jRadioButtonNotaSim.isSelected())
            money.setCupom(true);
        else
            money.setCupom(false);
    }//GEN-LAST:event_jButtonPayCashActionPerformed

    private void jButtonEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditActionPerformed
        select.setVisible(true);
    }//GEN-LAST:event_jButtonEditActionPerformed

    private void jButtonHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHistoryActionPerformed
        caderno.setVisible(true);        
    }//GEN-LAST:event_jButtonHistoryActionPerformed

    private void jButtonNew1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNew1ActionPerformed
        select.reset();
        habilitarCampos();
        select.setVisible(true);        
        if (select.getModel() != null) {
            jTableLista.setModel(select.getModel());            
        }
        value= Double.valueOf("0");
        jTextFieldPrice.setText(Double.toString(value));        
    }//GEN-LAST:event_jButtonNew1ActionPerformed

    private boolean validarInformacoes()
    {
        if (jTextFieldVender.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "O vendedor não foi informado!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldVender.requestFocus();
            return false;
        }
        
        if (jTextFieldClient.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "O cliente não foi informado!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldClient.requestFocus();
            return false;
        }
        
        if (jTextFieldNiver.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "A data de nascimento não foi informada!", "Operação",JOptionPane.PLAIN_MESSAGE);
            jTextFieldNiver.requestFocus();
            return false;
        }
        else
        {
            try
            {
                Date dataNascimento= dateFormat.parse(jTextFieldNiver.getText());
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, "A data de nascimento não é válida!", "Operação",JOptionPane.PLAIN_MESSAGE);
                jTextFieldNiver.setText(null);
                return false;
            }                        
        }
        
        return true;
    }
     
    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
       cancelarPedido();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void checkboxDescontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkboxDescontoMouseClicked
        if (checkboxDesconto.getState() == true)
        {   
            jRadioButton5Percent.setEnabled(true);
            jRadioButton10Percent.setEnabled(true);            
        }
        else
        {
            jRadioButton5Percent.setEnabled(false);
            jRadioButton10Percent.setEnabled(false);            
        }        
        applyDiscount();
    }//GEN-LAST:event_checkboxDescontoMouseClicked

    private void jButtonPayCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPayCardActionPerformed
        cartao.setVisible(true);
        paymentCondition= "CARD";
        currentCliente.setPaymentCondition(paymentCondition);
        cartao.limparCampos();
        cartao.setValue(Double.valueOf(jTextFieldPrice.getText()));
        if (jRadioButtonNotaSim.isSelected())
            cartao.setCupom(true);
        else
            cartao.setCupom(false);       
    }//GEN-LAST:event_jButtonPayCardActionPerformed
    
    private void applyDiscount()
    {
        Double price= value;
        if (checkboxDesconto.getState()==true)
            if (jRadioButton5Percent.isSelected())
            {
                currentCliente.setDesconto(Double.valueOf("0.95"));
                price= value*0.95;
                BigDecimal valor_real= new BigDecimal(price).setScale(2, RoundingMode.HALF_DOWN);
                price= Double.valueOf(valor_real.toString());
            }
            else
            {
                currentCliente.setDesconto(Double.valueOf("0.90")); 
                price= value*0.90;
                BigDecimal valor_real= new BigDecimal(price).setScale(2, RoundingMode.HALF_DOWN);
                price= Double.valueOf(valor_real.toString());
            }
        else
            currentCliente.setDesconto(Double.valueOf("1"));
        
        jTextFieldPrice.setText(price.toString());
    }
    
    private void jRadioButton5PercentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton5PercentActionPerformed
        applyDiscount();
    }//GEN-LAST:event_jRadioButton5PercentActionPerformed

    private void jRadioButton10PercentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton10PercentActionPerformed
        applyDiscount();
    }//GEN-LAST:event_jRadioButton10PercentActionPerformed

    private void jButtonCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCheckStockActionPerformed
        String informe= "Nosso estoque atual:\n";
        informe+= "Água: " + Double.toString(dispensa.numberOfAgua) + "\n";
        informe+= "Bolo: " + Double.toString(dispensa.numberOfBolo) + "\n";
        informe+= "Carne: " + Double.toString(dispensa.numberOfCarne) + "\n";
        informe+= "Cerveja: " + Double.toString(dispensa.numberOfCerveja) + "\n";
        informe+= "Coke: " + Double.toString(dispensa.numberOfCoke) + "\n";
        informe+= "Gelatina: " + Double.toString(dispensa.numberOfGelatina) + "\n";
        informe+= "Hamburger: " + Double.toString(dispensa.numberOfHamburger) + "\n";
        informe+= "Massa: " + Double.toString(dispensa.numberOfMassa) + "\n";
        informe+= "Águas: " + Double.toString(dispensa.numberOfPeixe) + "\n";
        informe+= "Pizza: " + Double.toString(dispensa.numberOfPizza) + "\n";
        informe+= "Sorvete: " + Double.toString(dispensa.numberOfSorvete) + "\n";
        informe+= "Suco: " + Double.toString(dispensa.numberOfSuco) + "\n";        
        informe+= "Torta: " + Double.toString(dispensa.numberOfTorta) + "\n";
        informe+= "Trufa: " + Double.toString(dispensa.numberOfTrufa) + "\n";
        informe+= "Vodka: " + Double.toString(dispensa.numberOfVodka);
        JOptionPane.showMessageDialog(null, informe, "Operação",JOptionPane.PLAIN_MESSAGE);             
    }//GEN-LAST:event_jButtonCheckStockActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Manager().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    protected static java.awt.Checkbox checkboxDesconto;
    private javax.swing.JButton jButtonAsk;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonCheckStock;
    private javax.swing.JButton jButtonEdit;
    private javax.swing.JButton jButtonHistory;
    private javax.swing.JButton jButtonNew1;
    protected static javax.swing.JButton jButtonPayCard;
    protected static javax.swing.JButton jButtonPayCash;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private static javax.swing.JLabel jLabelHora;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    protected static javax.swing.JRadioButton jRadioButton10Percent;
    protected static javax.swing.JRadioButton jRadioButton5Percent;
    private static javax.swing.JRadioButton jRadioButtonNotaNao;
    private static javax.swing.JRadioButton jRadioButtonNotaSim;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTableLista;
    private static javax.swing.JTextField jTextFieldAsk;
    protected static javax.swing.JTextField jTextFieldClient;
    private static javax.swing.JTextField jTextFieldDate;
    private static javax.swing.JTextField jTextFieldHour;
    protected static javax.swing.JTextField jTextFieldNiver;
    private static javax.swing.JTextField jTextFieldOK;
    protected static javax.swing.JTextField jTextFieldPrice;
    protected static javax.swing.JTextField jTextFieldVender;
    // End of variables declaration//GEN-END:variables
}
