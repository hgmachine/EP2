package controle;

import datamodel.Product;
import datamodel.Stock;
import tocomfomequeromais.Manager;

public class Checkout {
    
    private Stock contagem;
    
    public Checkout()
    {
        this.contagem= new Stock(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    }
    
    public void reset()
    {
        contagem.reset();
    }
    
    public void countItem(String item)
    {
        switch (item) {
            case "Hamburger":
               contagem.numberOfHamburger+=1;
                break;
            case "Pizza":
               contagem.numberOfPizza+=1;
                break;
            case "Carne":
                contagem.numberOfCarne+=1;
                break;
            case "Massa":
                contagem.numberOfMassa+=1;
                break;
            case "Peixe":
                contagem.numberOfPeixe+=1;
                break;
            case "Cerveja":
                contagem.numberOfCerveja+=1;
                break;
            case "Suco":
                contagem.numberOfSuco+=1;
                break;
            case "Coke":
                contagem.numberOfCoke+=1;
                break;
            case "Agua":
                contagem.numberOfAgua+=1;
                break;
            case "Vodka":
                contagem.numberOfVodka+=1;
                break;
            case "Bolo":
                contagem.numberOfBolo+=1;
                break;
            case "Torta":
                contagem.numberOfTorta+=1;
                break;
            case "Trufa":
                contagem.numberOfTrufa+=1;
                break;
            case "Sorvete":
                contagem.numberOfSorvete+=1;
                break;
            case "Gelatina":
                contagem.numberOfGelatina+=1;
                break;
        }       
    }
    
    public boolean checkItem(String item)
    {
        boolean check = false;
        switch (item) {
            case "Hamburger":
                if (Manager.dispensa.numberOfHamburger>=contagem.numberOfHamburger)
                    check= true;
                else
                    check= false;
                break;
            case "Pizza":
                if (Manager.dispensa.numberOfPizza>=contagem.numberOfPizza)
                    check= true;
                else
                    check= false;
                break;
            case "Carne":
                if (Manager.dispensa.numberOfCarne>=contagem.numberOfCarne)
                    check= true;
                else
                    check= false;
                break;
            case "Massa":
                if (Manager.dispensa.numberOfMassa>=contagem.numberOfMassa)
                    check= true;
                else
                    check= false;
                break;
          case "Peixe":
                if (Manager.dispensa.numberOfPeixe>=contagem.numberOfPeixe)
                    check= true;
                else
                    check= false;
                break;
            case "Cerveja":
                if (Manager.dispensa.numberOfCerveja>=contagem.numberOfCerveja)
                    check= true;
                else
                    check= false;
                break;
            case "Suco":
                if (Manager.dispensa.numberOfSuco>=contagem.numberOfSuco)
                    check= true;
                else
                    check= false;
                break;
            case "Coke":
                if (Manager.dispensa.numberOfCoke>=contagem.numberOfCoke)
                    check= true;
                else
                    check= false;
                break;
            case "Agua":
                if (Manager.dispensa.numberOfAgua>=contagem.numberOfAgua)
                    check= true;
                else
                    check= false;
                break;
            case "Vodka":
                if (Manager.dispensa.numberOfVodka>=contagem.numberOfVodka)
                    check= true;
                else
                    check= false;
                break;
            case "Bolo":
                if (Manager.dispensa.numberOfBolo>=contagem.numberOfBolo)
                    check= true;
                else
                    check= false;
                break;
            case "Torta":
                if (Manager.dispensa.numberOfTorta>=contagem.numberOfTorta)
                    check= true;
                else
                    check= false;
                break;
            case "Trufa":
                if (Manager.dispensa.numberOfTrufa>=contagem.numberOfTrufa)
                    check= true;
                else
                    check= false;
                break;
            case "Sorvete":
                if (Manager.dispensa.numberOfSorvete>=contagem.numberOfSorvete)
                    check= true;
                else
                    check= false;
                break;
            case "Gelatina":
                if (Manager.dispensa.numberOfGelatina>=contagem.numberOfGelatina)
                    check= true;
                else
                    check= false;
                break;
        }
        
        return check;
    }
    
    public void subtractItens()
    {
        Manager.dispensa.numberOfHamburger-= contagem.numberOfHamburger;
        Manager.dispensa.numberOfPizza-= contagem.numberOfPizza;
        Manager.dispensa.numberOfCarne-= contagem.numberOfCarne;
        Manager.dispensa.numberOfMassa-= contagem.numberOfMassa;
        Manager.dispensa.numberOfPeixe-= contagem.numberOfPeixe;
        Manager.dispensa.numberOfCerveja-= contagem.numberOfCerveja;
        Manager.dispensa.numberOfSuco-= contagem.numberOfSuco;
        Manager.dispensa.numberOfCoke-= contagem.numberOfCoke;
        Manager.dispensa.numberOfAgua-= contagem.numberOfAgua;
        Manager.dispensa.numberOfVodka-= contagem.numberOfVodka;
        Manager.dispensa.numberOfBolo-= contagem.numberOfBolo;
        Manager.dispensa.numberOfTorta-= contagem.numberOfTorta;
        Manager.dispensa.numberOfTrufa-= contagem.numberOfTrufa;
        Manager.dispensa.numberOfSorvete-= contagem.numberOfSorvete;
        Manager.dispensa.numberOfGelatina-= contagem.numberOfGelatina;
        this.reset();
    }
}
