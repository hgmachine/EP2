package datamodel;

public class Stock {

    public Integer numberOfHamburger;
    public Integer numberOfPizza;
    public Integer numberOfCarne;
    public Integer numberOfMassa;
    public Integer numberOfPeixe;

    public Integer numberOfCerveja;
    public Integer numberOfSuco;
    public Integer numberOfCoke;
    public Integer numberOfAgua;
    public Integer numberOfVodka;                
    
    public Integer numberOfBolo;
    public Integer numberOfTorta;
    public Integer numberOfTrufa;
    public Integer numberOfSorvete;
    public Integer numberOfGelatina;

    public Stock(Integer numberOfHamburger, Integer numberOfPizza, Integer numberOfCarne, Integer numberOfMassa, Integer numberOfPeixe, Integer numberOfCerveja, Integer numberOfSuco, Integer numberOfCoke, Integer numberOfAgua, Integer numberOfVodka, Integer numberOfBolo, Integer numberOfTorta, Integer numberOfTrufa, Integer numberOfSorvete, Integer numberOfGelatina) {
        this.numberOfHamburger = numberOfHamburger;
        this.numberOfPizza = numberOfPizza;
        this.numberOfCarne = numberOfCarne;
        this.numberOfMassa = numberOfMassa;
        this.numberOfPeixe = numberOfPeixe;
        this.numberOfCerveja = numberOfCerveja;
        this.numberOfSuco = numberOfSuco;
        this.numberOfCoke = numberOfCoke;
        this.numberOfAgua = numberOfAgua;
        this.numberOfVodka = numberOfVodka;
        this.numberOfBolo = numberOfBolo;
        this.numberOfTorta = numberOfTorta;
        this.numberOfTrufa = numberOfTrufa;
        this.numberOfSorvete = numberOfSorvete;
        this.numberOfGelatina = numberOfGelatina;
    }   

    public void reset()
    {
        numberOfHamburger = 0;
        numberOfPizza = 0;
        numberOfCarne = 0;
        numberOfMassa = 0;
        numberOfPeixe = 0;
        numberOfCerveja = 0;
        numberOfSuco = 0;
        numberOfCoke = 0;
        numberOfAgua = 0;
        numberOfVodka = 0;
        numberOfBolo = 0;
        numberOfTorta = 0;
        numberOfTrufa = 0;
        numberOfSorvete = 0;
        numberOfGelatina = 0;
    }
    
    public Integer getNumberOfHamburger() {
        return numberOfHamburger;
    }

    public void setNumberOfHamburger(Integer numberOfHamburger) {
        this.numberOfHamburger = numberOfHamburger;
    }

    public Integer getNumberOfPizza() {
        return numberOfPizza;
    }

    public void setNumberOfPizza(Integer numberOfPizza) {
        this.numberOfPizza = numberOfPizza;
    }

    public Integer getNumberOfCarne() {
        return numberOfCarne;
    }

    public void setNumberOfCarne(Integer numberOfCarne) {
        this.numberOfCarne = numberOfCarne;
    }

    public Integer getNumberOfMassa() {
        return numberOfMassa;
    }

    public void setNumberOfMassa(Integer numberOfMassa) {
        this.numberOfMassa = numberOfMassa;
    }

    public Integer getNumberOfPeixe() {
        return numberOfPeixe;
    }

    public void setNumberOfPeixe(Integer numberOfPeixe) {
        this.numberOfPeixe = numberOfPeixe;
    }

    public Integer getNumberOfCerveja() {
        return numberOfCerveja;
    }

    public void setNumberOfCerveja(Integer numberOfCerveja) {
        this.numberOfCerveja = numberOfCerveja;
    }

    public Integer getNumberOfSuco() {
        return numberOfSuco;
    }

    public void setNumberOfSuco(Integer numberOfSuco) {
        this.numberOfSuco = numberOfSuco;
    }

    public Integer getNumberOfCoke() {
        return numberOfCoke;
    }

    public void setNumberOfCoke(Integer numberOfCoke) {
        this.numberOfCoke = numberOfCoke;
    }

    public Integer getNumberOfAgua() {
        return numberOfAgua;
    }

    public void setNumberOfAgua(Integer numberOfAgua) {
        this.numberOfAgua = numberOfAgua;
    }

    public Integer getNumberOfVodka() {
        return numberOfVodka;
    }

    public void setNumberOfVodka(Integer numberOfVodka) {
        this.numberOfVodka = numberOfVodka;
    }

    public Integer getNumberOfBolo() {
        return numberOfBolo;
    }

    public void setNumberOfBolo(Integer numberOfBolo) {
        this.numberOfBolo = numberOfBolo;
    }

    public Integer getNumberOfTorta() {
        return numberOfTorta;
    }

    public void setNumberOfTorta(Integer numberOfTorta) {
        this.numberOfTorta = numberOfTorta;
    }

    public Integer getNumberOfTrufa() {
        return numberOfTrufa;
    }

    public void setNumberOfTrufa(Integer numberOfTrufa) {
        this.numberOfTrufa = numberOfTrufa;
    }

    public Integer getNumberOfSorvete() {
        return numberOfSorvete;
    }

    public void setNumberOfSorvete(Integer numberOfSorvete) {
        this.numberOfSorvete = numberOfSorvete;
    }

    public Integer getNumberOfGelatina() {
        return numberOfGelatina;
    }

    public void setNumberOfGelatina(Integer numberOfGelatina) {
        this.numberOfGelatina = numberOfGelatina;
    }
}
