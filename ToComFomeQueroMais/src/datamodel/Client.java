package datamodel;

public class Client extends Person {
    
    private String paymentCondition;
    private Double debt;
    private Double desconto;
    //private Integer shoppingNumber;
    
    public Client()
    {
        super();
        this.clear();
    }
    
    public void setPaymentCondition(String condition)
    {
        paymentCondition= condition;
    }
    
    public String getPaymentCondition()
    {
        return paymentCondition;
    }
    
    public void setDesconto(Double desconto) {
        desconto = desconto;
    }
    
    public Double getDesconto() {
        return desconto;
    }
    
    public void setDebt(Double debt)
    {
        debt= debt;
    }
    
    public Double getDebt()
    {
        return debt;
    }
    
    public void clear()
    {
        paymentCondition= "CASH";
        desconto= Double.valueOf("1");
        debt= Double.valueOf("0");         
    }
}
