package datamodel;

public interface Dessert {
    
    void setDessert(byte code, String description);
}
