package datamodel;

public class CashPayment extends Payment {

    private boolean paymentInCash;
    
    public void setReais(String reais) {
        paymentInCash= true;
        Double pago= Double.valueOf(reais);
        this.setPayment(pago);
    }
    
    public CashPayment()
    {
        super();
        paymentInCash= false;
    }
    
    public boolean PaymentInCash()
    {
        return paymentInCash;
    }    
}
