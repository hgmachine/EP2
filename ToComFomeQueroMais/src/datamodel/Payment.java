package datamodel;

public class Payment {

    private Double value;
    private Double paid;
    private boolean isPaid;
    private boolean cupom;

    public boolean isCupom() {
        return cupom;
    }

    public void setCupom(boolean cupom) {
        this.cupom = cupom;
    }
   
    public boolean getCupom() {
        return this.cupom;
    }
    // Client cliente;
    
    public Payment()
    {
        value= Double.valueOf("0");
        paid= Double.valueOf("0");
        cupom= false;
    }
    
    public void setValue(Double value)
    {
        this.value= value;
        paid= value;
    }
    
    public Double getValue()
    {
        return value;
    }
    
    public void setPayment(Double paid)
    {
        this.paid= paid;
        isPaid= true;
    }
    
    public Double getPayment()
    {
        return paid;
    }
    
    public boolean verifyPayment()
    {
        return isPaid && (paid >= value);
    }
    
    public Double GetChange()
    {
        if (verifyPayment()) { return paid - value; }
        else {
            return Double.valueOf("0");
        }
    }
}
