package datamodel;

import java.util.ArrayList;

public class Register {

    private ArrayList<Order> listaOrdens;
    private Integer numberOfsales;
    private Integer numberOfOK;

    public Register(Integer numberOfsales, Integer numberOfOK) {
        this.listaOrdens= new ArrayList<Order>();
        this.numberOfsales = numberOfsales;
        this.numberOfOK= numberOfOK;
    }
    
    public void setOneMoreSale()
    {
        numberOfsales+= 1;        
    }
    
    public void setOneMoreOk()
    {
        numberOfOK+= 1;        
    }
     
    public Integer getNumberOfSales()
    {
        return numberOfsales;
    }
    
    public Integer getNumberOfOK()
    {
        return numberOfOK;
    }
    
    public void addOrder(Order umaOrdem) {
        boolean situation;
        listaOrdens.add(umaOrdem);
        this.setOneMoreSale();
        if (umaOrdem.getPaymentCondition()=="CASH")
            situation= umaOrdem.getCashPayment().verifyPayment();
        else
            situation= umaOrdem.getCardPayment().verifyPayment();
        if (situation)
            this.setOneMoreOk();
    }
    
    public ArrayList<Order> getClientList() {
        return listaOrdens;
    }
    
    public void remClient(Client umaOrdem) {
        listaOrdens.remove(umaOrdem);
    }
    
    public Order searchClientName(String nome)
    {
        Client cliente;
        for (Order order: listaOrdens) {
            cliente= order.getCliente();
            if (cliente.getName().equalsIgnoreCase(nome)) {
                return order;
            }
        }
        return null;
    }

    public Order searchClientNiver(String niver)
    {
        Client cliente;
        for (Order order: listaOrdens) {
            cliente= order.getCliente();
            if (cliente.getNiver().equalsIgnoreCase(niver)) {
                return order;
            }
        }
        return null;
    }    
}
