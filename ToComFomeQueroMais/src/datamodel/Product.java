package datamodel;

public class Product implements Food, Drink, Dessert {
    
    // Opções:
    
    private final byte HAMBURGER = 1;
    private final byte PIZZA = 2;
    private final char CARNE = 3;
    private final char MASSA = 4;
    private final char PEIXE = 5;
    
    private final byte CERVEJA = 6;
    private final byte SUCO= 7;
    private final char COKE = 8;
    private final char AGUA = 9;
    private final char VODKA = 10;
    
    private final byte BOLO = 11;
    private final byte TORTA= 12;
    private final char TRUFA = 13;
    private final char SORVETE = 14;
    private final char GELATINA = 15;
    
    // 
    
    private Double price;
    private String item;
    private String preferences;

    public void insertGeneralItem(String name, String description)
    {
         switch (name) {
            case "Hamburger":
                setItem("Hamburger");
                break;
            case "Pizza":
                setItem("Pizza");
                break;
            case "Carne":
                setItem("Carne");
                break;
            case "Massa":
                setItem("Massa");
                break;
            case "Peixe":
                setItem("Peixe");
                break;
            case "Cerveja":
                setItem("Cerveja");
                break;
            case "Suco":
                setItem("Suco");
                break;
            case "Coke":
                setItem("Coke");
                break;
            case "Agua":
                setItem("Agua");
                break;
            case "Vodka":
                setItem("Vodka");
                break; 
            case "Bolo":
                setItem("Bolo");
                break;
            case "Torta":
                setItem("Torta");
                break;
            case "Trufa":
                setItem("Trufa");
                break;
            case "Sorvete":
                setItem("Sorvete");
                break;
            case "Gelatina":
                setItem("Gelatina");
                break;
         }
         setPreferences(description);
    }
    
    public Product()
    {
        item= "Não informado";
    }
    
    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }        
       
    public String getItem()
    {
        return item;
    }

    public Double getPrice() {
        return price;
    }

    public void setItem(String item)
    {
        this.item= item;
    }
    
    public void setPrice(Double price) {
        this.price = price;
    }
    
    @Override
    public void setFood(byte code, String description)
    {
        switch (code) {
            case HAMBURGER:
                setItem("Hamburger");
                break;
            case PIZZA:
                setItem("Pizza");
                break;
            case CARNE:
                setItem("Carne");
                break;
            case MASSA:
                setItem("Massa");
                break;
            case PEIXE:
                setItem("Peixe");
                break;
        }
        setPreferences(description);
    }
    
    @Override
    public void setDrink(byte code, String description)
    {
        switch (code) {
            case CERVEJA:
                setItem("Cerveja");
                break;
            case SUCO:
                setItem("Suco");
                break;
            case COKE:
                setItem("Coke");
                break;
            case AGUA:
                setItem("Agua");
                break;
            case VODKA:
                setItem("Vodka");
                break;
        }
        setPreferences(description);
    }
    
    @Override
    public void setDessert(byte code, String description)
    {
        switch (code) {
            case BOLO:
                setItem("Bolo");
                break;
            case TORTA:
                setItem("Torta");
                break;
            case TRUFA:
                setItem("Trufa");
                break;
            case SORVETE:
                setItem("Sorvete");
                break;
            case GELATINA:
                setItem("Gelatina");
                break;
        }
        setPreferences(description);
    }
}
