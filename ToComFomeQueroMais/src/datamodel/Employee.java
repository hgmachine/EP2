package datamodel;

public class Employee extends Person {
    
    private String salary;
    private Double sales;
    
    public Employee ()
    {
        super();
        sales= Double.valueOf("0");
    }
    
    public void incrementSales(Double vend)
    {
        sales+= vend;
    }

    public Double getSales() {
        return sales;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return Math.floor(sales.floatValue()/1000);
    }
}
