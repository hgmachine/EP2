package datamodel;

import java.util.ArrayList;
import javax.swing.DefaultListModel;

public class Order {
    
    private ArrayList<Product> listaProdutos;
    private Employee vendedor;
    private Client cliente;
    private String date;
    private String time;
    private Integer numberOfProducts;
    private CardPayment card;
    private CashPayment cash;
    
    public Order()
    {
        this.clear();
    } 

    public void clear()
    {
        listaProdutos= new ArrayList<Product>();
        vendedor= null;
        cliente= null;
        date= "??/??/????";
        time= "??h??m??s";
        numberOfProducts= 0;
        card= null;
        cash= null;
    }
    
    public void setCardPayment(CardPayment card)
    {
        cash= null;
        this.card= card;
    }
    
    public void setCashPayment(CashPayment cash)
    {
        card= null;
        this.cash= cash;
    }    
    
    public String getPaymentCondition()
    {
        if (cash==null)
            return "CARD";
        else if (card==null)
            return "CASH";
        else
            return "NONE";
    }
    
    public CashPayment getCashPayment()
    {
        return cash;
    }
    
    public CardPayment getCardPayment()
    {
        return card;
    }
    
    public Employee getVender() {
        return vendedor;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public void setVender(Employee vendedor) {
        this.vendedor = vendedor;
    }

    public void setOneMoreProduct()
    {
        numberOfProducts+= 1;        
    }
    
    public Integer getNumberOfProducts()
    {
        return numberOfProducts;
    }
    
    public void addProduct(Product umProduto) {
        listaProdutos.add(umProduto);
        this.setOneMoreProduct();
    }
    
    public ArrayList<Product> getClientList() {
        return listaProdutos;
    }
    
    public void remProduct(Client umProduto) {
        listaProdutos.remove(umProduto);
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
