package datamodel;

public class CardPayment extends Payment {
    
    private boolean paymentByCard;
    private String cardnumber;
    private String cardname;
    private String securitycode;
    
    public CardPayment ()
    {
        super();
        cardnumber= "0000-0000-0000-0000";
        cardname= "xxx";        
        securitycode= "000";
        paymentByCard= false;
    }
    
    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getCardname() {
        return cardname;
    }

    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    public String getSecuritycode() {
        return securitycode;
    }

    public void setSecuritycode(String securitycode) {
        this.securitycode = securitycode;
    }
    
    public boolean PaymentByCard()
    {
        return paymentByCard;
    }
}
